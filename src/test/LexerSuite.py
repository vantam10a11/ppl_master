import unittest
from TestUtils import TestLexer


class LexerSuite(unittest.TestCase):

    def test_lowercase_identifier(self):
        """test identifiers"""
        self.assertTrue(TestLexer.test("abc", "abc,<EOF>", 101))

    def test_lower_upper_id(self):
        self.assertTrue(TestLexer.test("aCBbdc", "aCBbdc,<EOF>", 102))

    def test_mixed_id(self):
        self.assertTrue(TestLexer.test("aAsVN3", "aAsVN3,<EOF>", 103))

    def test_integer(self):
        """test integers"""
        self.assertTrue(TestLexer.test("123a123", "123,a123,<EOF>", 104))

    def test_error_identifier(self):
        """"""
        self.assertTrue(TestLexer.test("@!a", "Error Token @", 105))

    def test_block_comment(self):
        """"""
        self.assertTrue(TestLexer.test("/* Block comment \n end block comment */", "<EOF>", 106))

    def test_block_comment_with_inner_line_comment(self):
        """"""
        self.assertTrue(TestLexer.test("/* Block comment #end block comment */", "<EOF>", 107))

    def test_block_comment_with_new_line_r(self):
        """"""
        self.assertTrue(TestLexer.test("/* Block comment \r end block comment */", "<EOF>", 108))

    def test_block_comment_with_nested_block_comment(self):
        """"""
        self.assertFalse(
            TestLexer.test("/* Block comment \n /* Block comment \n end block comment */ end block comment */", "<EOF>",
                           109))

    def test_block_comment_with_EOF(self):
        """"""
        self.assertTrue(TestLexer.test("/* Block comment <EOF> \n end block comment */", "<EOF>", 110))

    def test_inline_comment(self):
        """"""
        self.assertTrue(TestLexer.test("#line comment", "<EOF>", 111))

    def test_inline_comment_preceed_by_statment(self):
        """"""
        self.assertTrue(TestLexer.test("a#this is a line comment", "a,<EOF>", 112))

    def test_inline_comment_followed_by_statment(self):
        """"""
        self.assertTrue(TestLexer.test("#this is a line comment \na", "a,<EOF>", 113))

    def test_inline_comment_with_inner_block_comment(self):
        """"""
        self.assertTrue(TestLexer.test("#this is a line comment /* ab", "<EOF>", 114))

    def test_inline_comment_with_inner_block_comment_1(self):
        """"""
        self.assertTrue(TestLexer.test("#this is a line comment ab*/", "<EOF>", 115))

    def test_inline_comment_with_inner_block_comment_2(self):
        """"""
        self.assertTrue(TestLexer.test("#this is a line comment/* ab */", "<EOF>", 116))

    def test_float_without_exp(self):
        """"""
        self.assertTrue(TestLexer.test("1.0", "1.0,<EOF>", 117))

    def test_float_without_demical(self):
        """"""
        self.assertTrue(TestLexer.test("1e1", "1e1,<EOF>", 118))

    def test_float_full(self):
        """"""
        self.assertTrue(TestLexer.test("0.11E-4", "0.11E-4,<EOF>", 119))

    def test_float_full_1(self):
        """"""
        self.assertTrue(TestLexer.test("0.11e+4", "0.11e+4,<EOF>", 120))

    def test_float_error(self):
        """"""
        self.assertTrue(TestLexer.test(".5", ".,5,<EOF>", 121))

    def test_float_error_1(self):
        """"""
        self.assertTrue(TestLexer.test("1e", "1,e,<EOF>", 122))

    def test_float_demical_00(self):
        """"""
        self.assertTrue(TestLexer.test("0.00000", "0.00000,<EOF>", 123))

    def test_float_demical_00_1(self):
        """"""
        self.assertTrue(TestLexer.test("0.00001", "0.00001,<EOF>", 124))

    def test_float_full_without_unary(self):
        """"""
        self.assertTrue(TestLexer.test("2.0e5", "2.0e5,<EOF>", 125))

    def test_inline_comment_with_multi_line(self):
        """"""
        self.assertTrue(TestLexer.test("#this is a line comment\n#this is another line comment", "<EOF>", 126))

    def test_float_full_without_digit_demical(self):
        """"""
        self.assertTrue(TestLexer.test("7.e7", "7.e7,<EOF>", 127))

    def test_float_full_6(self):
        """"""
        self.assertTrue(TestLexer.test("11.0e5", "11.0e5,<EOF>", 128))

    def test_boolean_true(self):
        """"""
        self.assertTrue(TestLexer.test("true", "true,<EOF>", 129))

    def test_boolean_false(self):
        """"""
        self.assertTrue(TestLexer.test("false", "false,<EOF>", 130))

    def test_boolean_true_error(self):
        """"""
        self.assertFalse(TestLexer.test("True", "true,<EOF>", 131))

    def test_boolean_false_error(self):
        """"""
        self.assertFalse(TestLexer.test("False", "false,<EOF>", 132))

    def test_boolean_comment(self):
        """"""
        self.assertTrue(TestLexer.test("/*true*/", "<EOF>", 133))

    def test_boolean_boolean(self):
        """"""
        self.assertTrue(TestLexer.test("truetrue", "truetrue,<EOF>", 134))

    def test_boolean_boolean_1(self):
        """"""
        self.assertTrue(TestLexer.test("falsetrue", "falsetrue,<EOF>", 135))

    def test_boolean_id(self):
        """"""
        self.assertTrue(TestLexer.test("truea", "truea,<EOF>", 136))

    def test_string_empty(self):
        """"""
        self.assertTrue(TestLexer.test("""\"\"""", """\"\",<EOF>""", 137))

    def test_string_empty_nested_greedy(self):
        """"""
        self.assertTrue(TestLexer.test("""\"He asked me: \\"Where is John?\\"\"""",
                                       """\"He asked me: \\"Where is John?\\"\",<EOF>""", 138))

    def test_string_t(self):
        """"""
        self.assertTrue(TestLexer.test("""\"This is a string containing tab \t\"""",
                                       """\"This is a string containing tab \t\",<EOF>""", 139))

    def test_string_newline(self):
        """"""
        self.assertFalse(TestLexer.test("""\"This is a string containing tab \n\"""",
                                        """\"This is a string containing tab \n\",<EOF>""", 140))

    def test_string_contain_dqoute(self):
        """"""
        self.assertTrue(TestLexer.test("""\"He\\"John?\"""", """\"He\\"John?\",<EOF>""", 141))

    def test_string_contain_dqoute_odd(self):
        """"""
        self.assertTrue(TestLexer.test("""\"He\\"\\"\\"John?\"""", """\"He\\"\\"\\"John?\",<EOF>""", 142))

    def test_string_illegal_escape(self):
        """"""
        self.assertTrue(TestLexer.test("""\"A\\a\"""", """Illegal Escape In String: A\\a\"""", 143))

    def test_string_illegal_escape1(self):
        self.assertTrue(TestLexer.test("""\"\\aA\"""", """Illegal Escape In String: \\a""", 144))

    def test_string_illegal_escape2(self):
        self.assertTrue(TestLexer.test("""\"\\aA""", """Illegal Escape In String: \\a""", 145))

    def test_string_illegal_escape3(self):
        """"""
        self.assertTrue(TestLexer.test("""\"A\\a""", """Illegal Escape In String: A\\a""", 146))

    def test_string_un(self):
        """"""
        self.assertTrue(TestLexer.test("""\"A""", """Unclosed String: A""", 147))

    def test_string_un1(self):
        self.assertTrue(TestLexer.test("""\"A\t""", """Unclosed String: A\t""", 148))

    def test_string_un2(self):
        self.assertTrue(TestLexer.test("""\"A\n""", """Unclosed String: A""", 149))

    def test_string_un3(self):
        self.assertTrue(TestLexer.test("""\"A\r""", """Unclosed String: A""", 150))

    def test_string_un4(self):
        self.assertTrue(TestLexer.test("""\"A\f""", """Unclosed String: A\f""", 151))

    def test_string_un5(self):
        self.assertTrue(TestLexer.test("""\"A\\f""", """Unclosed String: A\\f""", 152))

    def test_string_11(self):
        self.assertTrue(TestLexer.test("""\"a\"""", """\"a\",<EOF>""", 153))

    def test_string_12(self):
        self.assertTrue(TestLexer.test("""\"a\";""", """\"a\",;,<EOF>""", 154))

    def test_separator_1(self):
        self.assertTrue(TestLexer.test("{a}", "{,a,},<EOF>", 155))

    def test_separator_2(self):
        self.assertTrue(TestLexer.test("[a]", "[,a,],<EOF>", 156))

    def test_separator_3(self):
        self.assertTrue(TestLexer.test("[[(:.,;)]]", "[,[,(,:,.,,,;,),],],<EOF>", 157))

    def test_separator_4(self):
        self.assertTrue(TestLexer.test("(1[+;2,=3)]", "(,1,[,+,;,2,,,=,3,),],<EOF>", 158))

    def test_separator_5(self):
        self.assertTrue(TestLexer.test("abc((123[asdsad))q,,weq.we;1231;];",
                                       "abc,(,(,123,[,asdsad,),),q,,,,,weq,.,we,;,1231,;,],;,<EOF>", 159))

    def test_separator_6(self):
        self.assertTrue(TestLexer.test("123fdf[[[asdsad]];asds;d,]][[qwewqr[",
                                       "123,fdf,[,[,[,asdsad,],],;,asds,;,d,,,],],[,[,qwewqr,[,<EOF>", 160))

    def test_integer1(self):
        """test integers"""
        self.assertTrue(TestLexer.test("123456a123456", "123456,a123456,<EOF>", 161))

    def test_integer2(self):
        """test integers"""
        self.assertTrue(TestLexer.test("0", "0,<EOF>", 162))

    def test_integer3(self):
        """test integers"""
        self.assertTrue(TestLexer.test("01", "0,1,<EOF>", 163))

    def test_integer4(self):
        """test integers"""
        self.assertTrue(TestLexer.test("-1", "-,1,<EOF>", 164))

    def test_integer5(self):
        """test integers"""
        self.assertTrue(TestLexer.test("+1", "+,1,<EOF>", 165))

    def test_integer6(self):
        """test integers"""
        self.assertTrue(TestLexer.test("1a23c4d5e6f7g8h9i", "1,a23c4d5e6f7g8h9i,<EOF>", 166))

    def test_intege7(self):
        """test integers"""
        self.assertTrue(TestLexer.test("1234567899876543210", "1234567899876543210,<EOF>", 167))

    def test_intege8(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 168))

    def test_intege9(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 169))

    def test_integ11(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 170))

    def test_integ12(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 171))

    def test_integ13(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 172))

    def test_integ14(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 173))

    def test_integ15(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 174))

    def test_integ16(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 175))

    def test_integ17(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 176))

    def test_integ18(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 177))

    def test_integ19(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 178))

    def test_integ179(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 179))

    def test_integ180(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 180))

    def test_integ181(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 181))

    def test_integ182(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 182))

    def test_integ183(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 183))

    def test_integ184(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 184))

    def test_integ185(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 185))

    def test_integ186(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 186))

    def test_integ187(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 187))

    def test_integ188(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 188))

    def test_integ189(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 189))

    def test_integ190(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 190))

    def test_integ191(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 191))

    def test_integ192(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 192))

    def test_integ193(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 193))

    def test_integ194(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 194))

    def test_integ195(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 195))

    def test_integ196(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 196))

    def test_integ197(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 197))

    def test_integ198(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 198))

    def test_integ199(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 199))

    def test_integ200(self):
        """test integers"""
        self.assertTrue(TestLexer.test("int a =1", "int,a,=,1,<EOF>", 200))