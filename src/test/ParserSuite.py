import unittest
from TestUtils import TestParser


class ParserSuite(unittest.TestCase):
    def test_simple_program(self):
        """Simple program"""
        input = """class ABC { }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 201))

    def test_more_complex_program(self):
        """More complex program"""
        input = """class Shape {
static final Shape[1] numOfShape = {this,nil};
}
"""
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 202))

    def test_wrong_miss_close(self):
        """Miss ) int main( {}"""
        input = """class Shape {"""
        expect = "Error on line 1 col 13: <EOF>"
        self.assertTrue(TestParser.test(input, expect, 203))

    def test_simple_program_extend(self):
        """Simple program"""
        input = """class ABC extends BCD { }"""
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 204))

    def test_simple_program_extend_error(self):
        """Simple program"""
        input = """class ABC extends { }"""
        expect = "Error on line 1 col 18: {"
        self.assertTrue(TestParser.test(input, expect, 205))

    def test_simple_program_error_miss_open(self):
        """Simple program"""
        input = """class { }"""
        expect = "Error on line 1 col 6: {"
        self.assertTrue(TestParser.test(input, expect, 206))

    def test_simple_program_error_not_clasname(self):
        """Simple program"""
        input = """class ABC }"""
        expect = "Error on line 1 col 10: }"
        self.assertTrue(TestParser.test(input, expect, 207))

    def test_simple_program_error_not_body(self):
        """Simple program"""
        input = """class A"""
        expect = "Error on line 1 col 7: <EOF>"
        self.assertTrue(TestParser.test(input, expect, 208))

    def test_simple_program_error_only_class(self):
        """Simple program"""
        input = """class"""
        expect = "Error on line 1 col 5: <EOF>"
        self.assertTrue(TestParser.test(input, expect, 209))

    def test_complex_program_final(self):
        """More complex program"""
        input = """class Shape {
                        final int numOfShape = 0;
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 210))

    def test_complex_program_static(self):
        """More complex program"""
        input = """class Shape {
                        static int numOfShape = 0;
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 211))

    def test_complex_program_final_static(self):
        """More complex program"""
        input = """class Shape {
                        final static int numOfShape = 0;
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 212))

    def test_complex_program_without_final_static(self):
        """More complex program"""
        input = """class Shape {
                        int numOfShape = 0;
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 213))

    def test_complex_program_1(self):
        """More complex program"""
        input = """class Shape {
                        static final int numOfShape = 0;
                        final int immuAttribute = 0;
                        float length,width;
                        static int getNumOfShape() {
                            return numOfShape;
                        }
                    }
                    class Rectangle extends Shape {
                        float getArea(){
                            return this.length*this.width;
                        }
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 214))

    def test_complex_program_attr_init_val(self):
        """More complex program"""
        input = """class Shape {
                        final int My1stCons = 1 + 5,My2ndCons = 2;
                        static int x,y = 0;
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 215))

    def test_complex_program_attr_init_val_concat_exp(self):
        """More complex program"""
        input = """class Shape {
                        final string My1stCons = \"a\"^\"b\";
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 216))

    def test_complex_program_attr_init_val_string(self):
        """More complex program"""
        input = """class A {
                        string a = \"a\";
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 217))

    def test_complex_program_attr_init_val_float(self):
        """More complex program"""
        input = """class A {
                        float a = 1.1;
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 218))

    def test_complex_program_5(self):
        input = """class A {
                        int a = --5;
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 219))

    def test_complex_program_6(self):
        input = """class A {
                        int[1] a = {1};
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 220))

    def test_complex_program_7(self):
        input = """class A {
                        int a = this.b;
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 221))

    def test_complex_program_8(self):
        input = """class A {
                        int a = this.b();
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 222))

    def test_complex_program_9(self):
        input = """class A {
                        int a = b[1][2];
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 223))

    def test_complex_program_91(self):
        input = """class A {
                        int[3] foo(){}
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 224))

    def test_complex_program_912(self):
        input = """class A {
                        int a = this.b(c);
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 225))

    def test_complex_program_913(self):
        input = """class A {
                        int a = this.b(c);
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 226))

    def test_complex_program_93(self):
        input = """class A {
                        int a = this.b(c);
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 227))

    def test_complex_program_193(self):
        input = """class A {
                        int a = a.b.c(a,b);
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 228))

    def test_complex_program_197(self):
        input = """class A {
                        int a =  a.b.c();
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 229))

    def test_complex_program_157(self):
        input = """class A {
                        int a = a.b.c;
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 230))

    def test_program_comment(self):
        input = """class A {
                        int a = this.b(c);
                        /* This is a block comment, that
may span in many lines*/
                    }
                """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 231))

    def test_program_comment_line_error(self):
        input = """class A {
                            a := 5;#this is a line comment
                        }
                    """
        expect = "Error on line 2 col 30: :="
        self.assertTrue(TestParser.test(input, expect, 232))

    def test_program_comment_line(self):
        input = """class A {
                            int a = 5;#this is a line comment
                        }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 233))

    def test_program_neg_int(self):
        input = """class A {
                            int a = -5;
                        }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 234))

    def test_program_pos_int(self):
        input = """class A {
                            int a = 5;
                        }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 235))

    def test_program_id(self):
        input = """class A {
                            int __DEV__;
                        }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 236))

    def test_program_id1(self):
        input = """class A {
                            int _0;
                        }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 237))

    def test_program_attr_decl(self):
        input = """class A {
                   int a,b,c;
                   int[100] a;
                        }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 238))

    def test_program_attr_decl1(self):
        input = """class A {
                   int a = 1,b = "hi",c;
                   int[100] a;
                        }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 239))

    def test_program_attr_decl2(self):
        input = """class A {
                   int a = 1,b = "hi",c;
                   int[100] a = {1,"hello",1.1};
                        }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 240))

    def test_program_attr_decl3(self):
        input = """class A {
                   int[100] a = {1,"hello",1.1}, b;
                        }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 241))

    def test_program_attr_decl4(self):
        input = """class A {
                   int[100] a = {1,"hello",1.1}, b = {1,"hello",1.1};
                        }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 242))

    def test_program_attr_decl4_error(self):
        input = """class A {
                   int[1 a;
                   }
                    """
        expect = "Error on line 2 col 25: a"
        self.assertTrue(TestParser.test(input, expect, 243))

    def test_program_attr_decl_bool(self):
        input = """class A {
                   boolean a = true;
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 244))

    def test_program_attr_decl_bool1(self):
        input = """class A {
                   boolean a = false;
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 245))

    def test_program_attr_decl_nil(self):
        input = """class A {
                   boolean a = nil;
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 246))

    def test_program_attr_decl_void(self):
        input = """class A {
                   void a = 1;
                   }
                    """
        expect = "Error on line 2 col 26: ="
        self.assertTrue(TestParser.test(input, expect, 247))

    def test_program_med_decl_void(self):
        input = """class A {
                   void a(){
                   }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 248))

    def test_program_med_decl_void1(self):
        input = """class A {
                   void a(){
                   int a = 0;
                   }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 249))

    def test_program_med_decl_void2(self):
        input = """class A {
                   void a(int a,b){
                   int a = 0;
                   }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 250))

    def test_program_med_decl_param(self):
        input = """class A {
                   int a(int a; float b,f){
                   }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 251))

    def test_program_med_decl_param1(self):
        input = """class A {
                   int a(int a; float b){
                   }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 252))

    def test_program_med_decl_param2(self):
        input = """class A {
                   int a(int a){
                   }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 253))

    def test_program_med_decl_constructor(self):
        input = """class A {
                        A(){
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 254))

    def test_program_med_decl_constructor1(self):
        input = """class A {
                        A(int a){
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 255))

    def test_program_med_decl_constructor2(self):
        input = """class A {
                        A(int a){
                            this.b := a;
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 256))

    def test_program_med_decl_assign_stmt(self):
        input = """class A {
                        A(){
                            a := 3 + 5;
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 257))

    def test_program_med_decl_assign_stmt2(self):
        input = """class A {
                        A(){
                            a := 3 >= 5;
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 258))

    def test_program_med_decl_assign_stmt3(self):
        input = """class A {
                        A(){
                            a := 3 == 5;
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 259))

    def test_program_med_decl_assign_stmt4(self):
        input = """class A {
                        A(){
                            a := 3 != 5;
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 260))

    def test_program_med_decl_assign_stmt5(self):
        input = """class A {
                        A(){
                            a := true;
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 261))

    def test_program_med_decl_assign_stmt6(self):
        input = """class A {
                        A(){
                            a := true && true;
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 262))

    def test_program_med_decl_assign_stmt2434(self):
        input = """class A {
                        A(){
                            a := 3 > 5;
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 263))

    def test_program_med_decl_assign_stmt6(self):
        input = """class A {
                        A(){
                            a := true && false;
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 264))

    def test_program_med_decl_assign_stmt7(self):
        input = """class A {
                        A(){
                            a := false && false;
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 265))

    def test_program_med_decl_assign_stmt8(self):
        input = """class A {
                        A(){
                            a := false || false;
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 266))

    def test_program_med_decl_assign_stmt9(self):
        input = """class A {
                        A(){
                            a := a \\ b;
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 267))

    def test_program_med_decl_assign_stmt11(self):
        input = """class A {
                        A(){
                            a := a % b;
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 268))

    def test_program_med_decl_assign_stmt10(self):
        input = """class A {
                        A(){
                            a := a / b;
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 269))

    def test_program_med_decl_assign_index(self):
        input = """class A {
                        A(){
                            a := a[a];
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 270))

    def test_program_med_decl_assign_index1(self):
        input = """class A {
                        A(){
                            a := a[100];
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 271))

    def test_program_med_decl_assign_index2(self):
        input = """class A {
                        A(){
                            a := a[a + b];
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 272))

    def test_program_med_decl_assign_index3(self):
        input = """class A {
                        A(){
                            a[3+x.foo(2)] := a[b[2]] +3;
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 273))

    def test_program_med_decl_assign_index4(self):
        input = """class A {
                        A(){
                            x.b[2] := x.m()[3];
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 274))

    def test_program_med_decl_blaock(self):
        input = """class A {
                        A(){
                           #start of declaration part
                float r,s;
                int[5] a,b;
                #list of statements
                r:=2.0;
                s:=r*r*this.myPI;
                a[0]:= s;
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 275))

    def test_program_med_decl_b22(self):
        input = """class A {
                        A(){
                     this.aPI := 3.14;
                  value := x.foo(5);
                    l[3] := value * 2;
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 276))

    def test_program_med_decl_b221(self):
        input = """class A {
                        A(){
               if flag then
                    io.writeStrLn("Expression is true");
               else
                    io.writeStrLn ("Expression is false");
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 277))

    def test_program_med_decl_b2ss21(self):
        input = """class A {
                        A(){
            for i := 1 to 100 do {
           io.writeIntLn(i);
           Intarray[i] := i + 1;
            }
             for x := 5 downto 2 do
           io.writeIntLn(x);
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 278))

    def test_program_med_invo(self):
        input = """class A {
                        A(){
                Shape.getNumOfShape();
                        }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 279))

    def test_program_ex1(self):
        input = """class A {
                   int factorial(int n){
                   if n == 0 then return 1; else return n * this.factorial(n - 1);
                   }
                   void main(){
                   int x;
                    x:= io.readInt();
                  io.writeIntLn(this.factorial(x));
                    }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 280))

    def test_program_ex1_1(self):
        input = """class A {
                   void main(){
                    }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 281))

    def test_program_ex2(self):
        input = """class A {
                   float length,width;
                                                    float getArea() {}
                        Shape(float length,width){
                    this.length := length;
                    this.width := width;
                    }
                }
                    class Rectangle extends Shape {
                float getArea(){
            return this.length*this.width;
                }
                }
                class Triangle extends Shape {
                float getArea(){
                    return this.length*this.width / 2;
            }
                }
            class Example2 {
                void main(){
                s := new Rectangle(3,4);
                io.writeFloatLn(s.getArea());
                s := new Triangle(3,4);
                io.writeFloatLn(s.getArea());
                }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 282))

    def test_if_no_then_keyword(self):
        input = """class A {
                   void main(){
                            if(a == b) a:=1;
                    }
                   }
                    """
        expect = "successful"
        self.assertFalse(TestParser.test(input, expect, 283))

    def test_method_decl_nested(self):
        input = """class A {
                   void main(){
                        int abc(){
                        }
                    }
                   }
                    """
        expect = "successful"
        self.assertFalse(TestParser.test(input, expect, 284))

    def test_if_no_statment_then_keyword(self):
        input = """class A {
                   void main(){
                            if(a == b) then;
                    }
                   }
                    """
        expect = "successful"
        self.assertFalse(TestParser.test(input, expect, 285))

    def test_if_just_else_keyword(self):
        input = """class A {
                   void main(){
                            if(a == b) else a:=0;
                    }
                   }
                    """
        expect = "successful"
        self.assertFalse(TestParser.test(input, expect, 286))

    def test_if_block_statement(self):
        input = """class A {
                   void main(){
                            if(a == b) then {
                            int a = 0;
                            }
                    }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 287))

    def test_if_block_statement_end_semi_error(self):
        input = """class A {
                   void main(){
                            if(a == b) then {
                            int a = 0;
                            };
                    }
                   }
                    """
        expect = "successful"
        self.assertFalse(TestParser.test(input, expect, 288))

    def test_if_block_statement_else_too(self):
        input = """class A {
                   void main(){
                            if(a == b) then {
                            int a = 0;
                            } else {
                            int a = 0;
                            }
                    }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 289))

    def test_if_nested(self):
        input = """class A {
                   void main(){
                            if(a == b) then 
                                if (d == c) then a :=1;
                    }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 290))

    def test_if_else_nested(self):
        input = """class A {
                   void main(){
                            if(a == b) then 
                                if (d == c) then a :=1;
                                else b:=0;
                            else c:=0;
                    }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 291))

    def test_else_if_nested(self):
        input = """class A {
                   void main(){
                            if(a == b) then 
                                if (d == c) then a :=1;
                                else b:=0;
                            else if (d == c) then 
                                    if (d == c) then a :=1;
                                    else b:=0;
                                else b:=0;
                    }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 292))

    def test_if_and_op(self):
        input = """class A {
                   void main(){
                            if(a == b && d) then a :=1;
                    }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 293))

    def test_if_or_op(self):
        input = """class A {
                   void main(){
                            if(!a || d) then a :=1;
                    }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 294))

    def test_index_op(self):
        input = """class A {
                   void main(){
                           a[a+b] := 0;
                    }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 295))

    def test_if_less_op_equal(self):
        input = """class A {
                   void main(){
                            if(a < b && d == c) then a :=1;
                    }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 296))

    def test_if_less_op_equal1(self):
        input = """class A {
                   void main(){
                            if(a == b && d || c) then a :=1;
                    }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 297))

    def test_if_less_op_equal2(self):
        input = """class A {
                   void main(){
                            if((a != b) && (d == c)) then a :=1;
                    }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 298))

    def test_if_else_nested_else_of_which_if(self):
        input = """class A {
                   void main(){
                            if a == b then if a == b then a:= 1;
                            else b:=1;
                    }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 299))

    def test_if_else_nested_else_of_which_if1(self):
        input = """class A {
                   void main(){
                        b:=1;
                    }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 300))

    def test_if_else_nested_else_of_which_if12(self):
        input = """class A {
                   void main(){
                        b:=1;
                    }
                   }
                    """
        expect = "successful"
        self.assertTrue(TestParser.test(input, expect, 2991))