//==========================================================
// 1713057 - TRAN VAN TAM
//==========================================================

grammar BKOOL;

@lexer::header {
from lexererr import *
}

options{
	language=Python3;
}
//.....................................Parser....................................//
program  : classDeclarationList EOF ;

classDeclarationList: classDeclaration | classDeclaration classDeclarationList  ;

classDeclaration: CLASS ID (EXTENDS ID)? LP classMemberList? RP;

classMemberList: classMember classMemberList | classMember;

classMember: classAttribute | classMethod;

classAttribute: (FINAL|STATIC|FINAL STATIC| STATIC FINAL|) attrDecl SEMI;

attrDecl: attrType attrList;

attrList: attr COMMA attrList | attr ;

attr: ID EQUAL exp | ID ;

scalarType: INTTYPE | FLOATTYPE | STRINGTYPE | BOOLTYPE;

attrType: scalarType | arrayType | classType;

classType: ID;

arrayType: (scalarType | classType) LSB INTLIT RSB;

arrayLit: LP literalList RP;

literalList: expLiteral COMMA literalList | expLiteral;

expLiteral: INTLIT | FLOATLIT | STRINGLIT | BOOLEANLIT | THIS | NIL;

signOp: ADD | SUB;

methodType:  VOIDTYPE | attrType ;

classMethod: STATIC? methodType? ID LB parameterList? RB blockStmt ;

parameterList: paramDecl SEMI parameterList | paramDecl;

paramDecl: attrType idListInParam;

idListInParam: ID COMMA idListInParam | ID;

statementList: statement statementList | statement ;
//
//statement: blockStmt | assignStmt
//         | ifStmt
//         | forStmt
//         | breakStmt
//         | continueStmt
//         | methodInvocationStmt
//         | returnStmt;
statement: matchStmt | unMatchStmt;

matchStmt: matchIfStmt | blockStmt | assignStmt | forStmt | breakStmt| continueStmt| methodInvocationStmt| returnStmt;

matchIfStmt: IF exp THEN matchStmt ELSE matchStmt;

unMatchStmt: (IF exp THEN statement) | (IF exp THEN matchStmt ELSE unMatchStmt);

blockStmt: LP varDeclList? statementList? RP;

varDeclList: varDecl varDeclList | varDecl;

varDecl: FINAL? attrDecl SEMI;

assignStmt: lhs EQUAL_ASSIGN exp SEMI;

breakStmt: BREAK SEMI;

continueStmt: CONTINUE SEMI;

returnStmt: RETURN exp SEMI;

lhs: ID | indexExpression | memberAccessExp;

//ifStmt: IF exp THEN statement (ELSE statement)?;

forStmt: FOR ID EQUAL_ASSIGN exp (TO | DOWNTO) exp DO statement;

methodInvocationStmt: exp DOT ID LB expList? RB SEMI;

memberAccessExp: exp DOT ID;

indexExpression : exp LSB exp RSB;

expList: exp (COMMA exp)* ;

//Expression
exp: exp1 (LESS_THAN | MORE_THAN | LESS_OR_EQUAL | MORE_OR_EQUAL) exp1 | exp1 ;
exp1: exp2  (EQUAL_COMP | NOT_EQUAL ) exp2 | exp2;
exp2: exp2  (AND | OR ) exp3 | exp3;
exp3: exp3 signOp exp4 | exp4;
exp4: exp4 (MUL | FLOAT_DIV | INT_DIV | INT_MOD) exp5 | exp5;
exp5: exp5 CONCAT_STR exp6 | exp6;
exp6: NOT exp6 | exp7; //confỉrm NOT
exp7: signOp exp7 | exp8;
exp8: exp8 LSB exp RSB | exp9;
exp9: exp9 DOT ID (LB expList? RB)? | exp10;
exp10: NEW ID LB expList? RB | exp11;
exp11: ID | expLiteral | arrayLit | LB exp RB ;

//.....................................LEXER.....................................//
//Comment
COMMENT: (BLOCKCOMMENT | LINECOMMENT) -> skip;
fragment BLOCKCOMMENT: STARTPARTBLOCKCOMMENT .*? ENDTPARTBLOCKCOMMENT ;
fragment LINECOMMENT: STARTPARTLINECOMMENT .*? ENDPARTLINECOMMENT;
fragment STARTPARTBLOCKCOMMENT: '/*';
fragment ENDTPARTBLOCKCOMMENT: '*/';
fragment STARTPARTLINECOMMENT: '#';
fragment ENDPARTLINECOMMENT: EOF | '\n' | '\r';

//Literals

INTLIT: '0' | POSINT;
fragment DIGIT: [0-9];
fragment POSINT: [1-9] DIGIT*;

FLOATLIT: INTPART ( (DECMICALPART EXPPART) | DECMICALPART | EXPPART );
fragment INTPART: INTLIT;
fragment DECMICALPART: DOT DIGIT*;
fragment EXPPART: [Ee] (ADD | SUB)? INTLIT;

BOOLEANLIT: TRUE | FALSE;

STRINGLIT: DOUBLEQ ( ESCAPE_SEQUENCES | ~[\n\r\\"] )* DOUBLEQ;
fragment DOUBLEQ : '"';
fragment ESCAPE_SEQUENCES: '\\' [bfrnt"\\] ;

//Keywords
TRUE: 'true';

FALSE: 'false';

BREAK : 'break';

CONTINUE : 'continue';

DO : 'do';

ELSE : 'else';

FOR : 'for';

IF : 'if';

RETURN : 'return';

THEN : 'then';

WHILE : 'While';

CLASS: 'class';

EXTENDS: 'extends';

STATIC: 'static';

FINAL: 'final';

NEW: 'new';

NIL: 'nil';

THIS: 'this';

TO: 'to';

DOWNTO: 'downto';

//Type
INTTYPE: 'int' ;

FLOATTYPE: 'float' ;

VOIDTYPE: 'void'  ;

STRINGTYPE: 'string';

BOOLTYPE: 'boolean';
//Operators
EQUAL: '=';

EQUAL_ASSIGN: ':=';

ADD: '+'; //Addition or unary plus

SUB: '-'; //Subtraction or minus

MUL : '*';

INT_DIV : '\\';

FLOAT_DIV : '/';

INT_MOD : '%';

NOT : '!';

AND : '&&';

OR : '||';

EQUAL_COMP : '==';

NOT_EQUAL : '!=';

LESS_THAN : '<';

MORE_THAN : '>';

LESS_OR_EQUAL : '<=';

MORE_OR_EQUAL : '>=';

CONCAT_STR: '^';

//Separators

LB: '(' ;

RB: ')' ;

LSB: '[';

RSB: ']';

COLON: ':' ;

DOT: '.';

COMMA: ',';

LP: '{';

RP: '}';

SEMI: ';' ;

//Identifiers

ID: [a-zA-Z_][a-zA-Z0-9_]* ;

WS : [ \f\t\r\n]+ -> skip ; // skip spaces, tabs, newlines


ILLEGAL_ESCAPE: DOUBLEQ (ESCAPE_SEQUENCES | ~[\n\r\\"])* ('\\' ~[bfrnt"\\]) DOUBLEQ?
        {
           raise IllegalEscape(self.text[1:])
        };

UNCLOSE_STRING: DOUBLEQ ( ESCAPE_SEQUENCES | ~[\n\r\\"] )*
        {
            if self.text[-1]=='\n':
                 raise UncloseString(self.text[1:-1])
            else:
                raise UncloseString(self.text[1:])
        }
    ;
ERROR_CHAR: . { raise ErrorToken(self.text)};